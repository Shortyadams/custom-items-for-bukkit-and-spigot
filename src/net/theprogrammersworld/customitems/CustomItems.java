package net.theprogrammersworld.customitems;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import net.theprogrammersworld.customitems.commands.CmdExecutor;
import net.theprogrammersworld.customitems.data.PlayerData;
import net.theprogrammersworld.customitems.item.ItemHandler;
import net.theprogrammersworld.customitems.item.Recipes;
import net.theprogrammersworld.customitems.misc.Util;
import net.theprogrammersworld.customitems.Telemetry;

public class CustomItems extends JavaPlugin implements Listener{

	public String version="1.2.0";
	public String build="0003";
	private ConfigDB configDB=null;
	private ItemHandler itemHandler=null;
	private static API api=null;
	private PlayerData playerData=null;
	private Recipes recipes=null;
	private static CustomItems instance=null;
	
	private Logger log = Logger.getLogger("Minecraft");
	
	public void onEnable(){
		
		// Init
		
		instance=this;
		configDB=new ConfigDB(this);
		itemHandler=new ItemHandler(this);
		api=new API(this);
		playerData=new PlayerData(this);
		recipes=new Recipes(this);
		
		// Config startup
		
		configDB.Startup();
		
		// Register listeners
		
		getServer().getPluginManager().registerEvents((Listener) new net.theprogrammersworld.customitems.listeners.BlockListener(this), this);
		getServer().getPluginManager().registerEvents((Listener) new net.theprogrammersworld.customitems.listeners.EntityListener(this), this);
		getServer().getPluginManager().registerEvents((Listener) new net.theprogrammersworld.customitems.listeners.PlayerListener(this), this);
		
		// Set command executor
		this.getCommand("ci").setExecutor((CommandExecutor) new CmdExecutor(this));
		
		log.info(Util.editConsole("Plugin loaded! Version "+version+" / Build: "+build));
		
		// Start the telemetry collection.
		new Thread(new Telemetry()).start();
		
		// Check if the user is running the latest version of the plugin and display
		// a message in the server console if they are not.
		ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
		try {
			URL versionCheckURL = new URL("https://www.theprogrammersworld.net/CustomItems/latestVersion.html");
			BufferedReader remoteNumberReader = new BufferedReader(new InputStreamReader(versionCheckURL.openStream()));
			String remoteVersionNumber = remoteNumberReader.readLine();
			if(!remoteVersionNumber.equals(build))
				console.sendMessage(ChatColor.RED + "A new version of Custom Items is available.\nTo get it, " +
						"go to www.theprogrammersworld.net/CustomItems and click \"Download\".");
		} catch (Exception e) {
			console.sendMessage(ChatColor.RED + "Custom Items was unable to connect to the internet to check\n" +
					"for a new version.");
		}
	}
	
	public void onDisable(){	
		log.info(Util.editConsole("Plugin stopped!"));
	}
	
	public boolean hasPermission(Player player,String str){
		if (player.hasPermission(str)){
			return true;
		}else if (player.hasPermission("custom-items.*")){
			return true;
			}
		return false;
	}
	
	public ConfigDB getConfigDB(){
		return this.configDB;
	}
	
	public ItemHandler getItemHandler(){
		return this.itemHandler;
	}
	
	public Logger getLog(){
		return this.log;
	}
	
	public static API getAPI(){
		return api;
	}
	
	public PlayerData getPlayerData(){
		return this.playerData;
	}
	
	public Recipes getRecipes(){
		return this.recipes;
	}
	
	public static CustomItems getInstance(){
	    return instance;
	}
	
}
