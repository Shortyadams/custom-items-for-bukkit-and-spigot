package net.theprogrammersworld.customitems;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.bukkit.Bukkit;

public class Telemetry implements Runnable {

	@Override
	public void run() {
		// In order to collect more information about who is using Custom Items, we are going
		// to start collecting data about the servers that are running it. This is going to
		// be done as part of an effort to build more specific promotional material.
		
		// Put the thread to sleep for one minute before we run initial telemetry collection.
		// After the initial run, telemetry collection will only occur once every hour.
		try {
			Thread.sleep(60000);
		} catch (Exception e) {}
		
		while(true) {
			// Telemetry collection time!			
			try {
				// Get the version number of the Minecraft server.
				String serverVersion = Bukkit.getServer().getVersion();
				
				// Get the port on which the Minecraft server is running.
				String serverPortNumber = Integer.toString(Bukkit.getServer().getPort());
				
				// Get the plugin's version number.
				String pluginVersionNumber = Bukkit.getServer().getPluginManager().getPlugin("Custom_Items").
						  getDescription().getVersion();
				
				// Get the Custom Items configuration file and get a string from it that we can POST
				// to the server.
				File configFile = new File("plugins" + File.separator + "CustomItems" + File.separator + "config.yml");
				FileInputStream configFileInputStream = new FileInputStream(configFile);
				byte[] configFileBytes = new byte[(int) configFile.length()];
				configFileInputStream.read(configFileBytes);
				configFileInputStream.close();
				String configFileString = new String(configFileBytes, "UTF-8");
				
				// Get the Custom Items items file and get a string from it that we can POST to the server.
				File itemsFile = new File("plugins" + File.separator + "CustomItems" + File.separator + "items.yml");
				FileInputStream itemsFileInputStream = new FileInputStream(itemsFile);
				byte[] itemsFileBytes = new byte[(int) itemsFile.length()];
				itemsFileInputStream.read(itemsFileBytes);
				itemsFileInputStream.close();
				String itemsFileString = new String(itemsFileBytes, "UTF-8");
				
				// Get the Custom Items recipes file and get a string from it that we can POST to the server.
				File recipesFile = new File("plugins" + File.separator + "CustomItems" + File.separator + "recipes.yml");
				FileInputStream recipesFileInputStream = new FileInputStream(recipesFile);
				byte[] recipesFileBytes = new byte[(int) recipesFile.length()];
				recipesFileInputStream.read(recipesFileBytes);
				recipesFileInputStream.close();
				String recipesFileString = new String(recipesFileBytes, "UTF-8");
				
				// POST the collected data to the server.
				String postData = URLEncoder.encode("serverVersion", "UTF-8") + "=" + URLEncoder.encode(serverVersion, "UTF-8") +
						"&" + URLEncoder.encode("serverPortNumber", "UTF-8") + "=" + URLEncoder.encode(serverPortNumber, "UTF-8") +
						"&" + URLEncoder.encode("pluginVersionNumber", "UTF-8") + "=" + URLEncoder.encode(pluginVersionNumber, "UTF-8") +
						"&" + URLEncoder.encode("configFile", "UTF-8") + "=" + URLEncoder.encode(configFileString, "UTF-8") +
						"&" + URLEncoder.encode("itemsFile", "UTF-8") + "=" + URLEncoder.encode(itemsFileString, "UTF-8") +
						"&" + URLEncoder.encode("recipesFile", "UTF-8") + "=" + URLEncoder.encode(recipesFileString, "UTF-8");
				URL telemetryURL = new URL("https://theprogrammersworld.net/CustomItems/pluginTelemetry.php");
				URLConnection urlConn = telemetryURL.openConnection();
				urlConn.setDoOutput(true);
				OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
				wr.write(postData);
				wr.flush();
				urlConn.getInputStream();
				
				// Wait one hour before we do this again.
				Thread.sleep(3600000);
			} catch (Exception e) {}
		}
	}

}
