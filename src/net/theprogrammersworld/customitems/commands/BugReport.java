package net.theprogrammersworld.customitems.commands;

import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BugReport {
	public static String sendBugReport(Player player, String[] messageWords) {
		// Collect the necessary information and POST it to the server where bug reports will be
		// collected. Based on the result of the submission, return a string.
		// (0 = report failed, 1 = unofficial version, 2 = unsupported version, 3 = success,
		//  4 = server IP banned, 5 = UUID banned)
		String checksum = computeMD5();
		String port = Integer.toString(Bukkit.getServer().getPort());
		String version = Bukkit.getServer().getPluginManager().getPlugin("Custom_Items").
				getDescription().getVersion();
		String playerUUID;
		String message = "";
		try {
			playerUUID = player.getUniqueId().toString();
		} catch (Exception e) {
			playerUUID = "CONSOLE";
		}
		
		// Assemble the array of words in to a single message.
		for(int x = 1; x < messageWords.length; x++) {
			message += messageWords[x] + " ";
		}
		
		try {
			// Get the contents of the configuration file.
			File configFile = new File("plugins" + File.separator + "CustomItems" + File.separator + "config.yml");
			FileInputStream configFileInputStream = new FileInputStream(configFile);
			byte[] configFileBytes = new byte[(int) configFile.length()];
			configFileInputStream.read(configFileBytes);
			configFileInputStream.close();
			String configFileString = new String(configFileBytes, "UTF-8");
			
			// Get the contents of the items file.
			File itemsFile = new File("plugins" + File.separator + "CustomItems" + File.separator + "items.yml");
			FileInputStream itemsFileInputStream = new FileInputStream(itemsFile);
			byte[] itemsFileBytes = new byte[(int) itemsFile.length()];
			itemsFileInputStream.read(itemsFileBytes);
			itemsFileInputStream.close();
			String itemsFileString = new String(itemsFileBytes, "UTF-8");
			
			// Get the contents of the recipes file.
			File recipesFile = new File("plugins" + File.separator + "CustomItems" + File.separator + "recipes.yml");
			FileInputStream recipesFileInputStream = new FileInputStream(recipesFile);
			byte[] recipesFileBytes = new byte[(int) recipesFile.length()];
			recipesFileInputStream.read(recipesFileBytes);
			recipesFileInputStream.close();
			String recipesFileString = new String(recipesFileBytes, "UTF-8");
			
			// After all of the necessary information has been acquired, submit the report.
			String postData = URLEncoder.encode("uuid", "UTF-8") + "=" + URLEncoder.encode(playerUUID, "UTF-8") +
					"&" + URLEncoder.encode("port", "UTF-8") + "=" + URLEncoder.encode(port, "UTF-8") +
					"&" + URLEncoder.encode("version", "UTF-8") + "=" + URLEncoder.encode(version, "UTF-8") +
					"&" + URLEncoder.encode("message", "UTF-8") + "=" + URLEncoder.encode(message, "UTF-8") +
					"&" + URLEncoder.encode("checksum", "UTF-8") + "=" + URLEncoder.encode(checksum, "UTF-8") +
					"&" + URLEncoder.encode("configFile", "UTF-8") + "=" + URLEncoder.encode(configFileString, "UTF-8") +
					"&" + URLEncoder.encode("itemsFile", "UTF-8") + "=" + URLEncoder.encode(itemsFileString, "UTF-8") +
					"&" + URLEncoder.encode("recipesFile", "UTF-8") + "=" + URLEncoder.encode(recipesFileString, "UTF-8");
			URL submitURL = new URL("https://theprogrammersworld.net/CustomItems/pluginBugReporter.php");
			URLConnection urlConn = submitURL.openConnection();
			urlConn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream());
			wr.write(postData);
			wr.flush();
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
			String serverResponse;
			while((serverResponse = rd.readLine()) != null) {
				if(serverResponse.equals("1")) {
					// Report Failed - Unofficial Version
					rd.close();
					wr.close();
					return "1";
				}
				else if(serverResponse.equals("2")) {
					// Report Failed - Unsupported Version
					rd.close();
					wr.close();
					return "2";
				}
				else if(serverResponse.equals("3")) {
					// Report Successful - Get the Report ID
					String reportID = rd.readLine();
					rd.close();
					wr.close();
					return reportID;
				}
				else if(serverResponse.equals("4")) {
					// Report Failed - Server is Banned
					rd.close();
					wr.close();
					return "4";
				}
				else if(serverResponse.equals("5")) {
					// Report Failed - Player UUID is Banned
					rd.close();
					wr.close();
					return "5";
				}
			}
		} catch (Exception e){}
		// The report failed because for some reason, the server could not be reached.
		return "0";
	}
	
	private static String computeMD5() {
		// Determine the MD5 hash of the plugin.
		File pluginFile = new File(BugReport.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        try
        {
        	String filepath = URLDecoder.decode(pluginFile.getAbsolutePath(), StandardCharsets.UTF_8.toString());
            StringBuilder hash = new StringBuilder();
            MessageDigest md = MessageDigest.getInstance("MD5");
            FileInputStream fis = new FileInputStream(filepath);
            byte[] dataBytes = new byte[1024];
            int nread = 0; 

            while((nread = fis.read(dataBytes)) != -1)  
                 md.update(dataBytes, 0, nread);

            byte[] mdbytes = md.digest();

            for(int i=0; i<mdbytes.length; i++)
            hash.append(Integer.toString((mdbytes[i] & 0xff) + 0x100 , 16).substring(1));
            fis.close();
            return hash.toString();
        }
        catch(Exception e)
        {
        	return "";
        }
	}
}
