package net.theprogrammersworld.customitems.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import net.theprogrammersworld.customitems.CustomItems;

public class PlayerListener implements Listener{
	
	private CustomItems instance=null;
	
	public PlayerListener(CustomItems i){
		instance=i;
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event){
		if (event.getPlayer()!=null){
			if(event.getClickedBlock()!=null){
				if(event.getAction() == Action.LEFT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_BLOCK){
					
					if (event.getPlayer().getInventory().getItemInMainHand()!=null){
						if (event.getPlayer().getInventory().getItemInMainHand().getType()!=Material.BOW && event.getPlayer().getInventory().getItemInMainHand().getType()!=Material.SNOW_BALL){
						if(instance.getItemHandler().hasAbility(event.getPlayer().getInventory().getItemInMainHand(), "Lightning") && instance.getItemHandler().hasPermission(event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand())){
							event.getClickedBlock().getLocation().getWorld().strikeLightning(event.getClickedBlock().getLocation());
						}
						if(instance.getItemHandler().hasAbility(event.getPlayer().getInventory().getItemInMainHand(), "Fire") && instance.getItemHandler().hasPermission(event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand())){
							Location loc = event.getClickedBlock().getLocation();
						    loc.setY(loc.getY()+1);
						    if (loc.getWorld().getBlockAt(loc).getType()==Material.AIR){
						    	loc.getWorld().getBlockAt(loc).setType(Material.FIRE);
						    }
						}
						if(instance.getItemHandler().hasAbility(event.getPlayer().getInventory().getItemInMainHand(), "Teleport") && instance.getItemHandler().hasPermission(event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand())){
							Location loc = event.getClickedBlock().getLocation();
							loc.setPitch(event.getPlayer().getLocation().getPitch());
							loc.setYaw(event.getPlayer().getLocation().getYaw());
							loc.setY(loc.getY()+1);
							event.getPlayer().teleport(loc);
							
						}
						if(instance.getItemHandler().hasAbility(event.getPlayer().getInventory().getItemInMainHand(), "Explosion") && instance.getItemHandler().hasPermission(event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand())){
							Location loc = event.getClickedBlock().getLocation();
							loc.getWorld().createExplosion(loc, 4);
						}
						}
						
					}
					
				}
			}
		}
	}
	
}
