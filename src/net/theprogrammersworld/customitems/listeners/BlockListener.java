package net.theprogrammersworld.customitems.listeners;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;

import net.theprogrammersworld.customitems.CustomItems;

public class BlockListener implements Listener{

	private CustomItems instance=null;
	
	public BlockListener(CustomItems i){
		instance=i;
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		if (event.getPlayer()!=null){
			if (event.getBlock()!=null){
				if (event.getPlayer().getInventory().getItemInMainHand()!=null){
					if (instance.getItemHandler().hasAbility(event.getPlayer().getInventory().getItemInMainHand(), "SuperFortune") && instance.getItemHandler().hasPermission(event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand())){
						int chance=new Random().nextInt(5);
						if (chance!=0){
							
							if (!event.isCancelled())
							{
								int i=0;
								for(i=0;i<=chance;i++){
									if (event.getBlock().getDrops()!=null){
										if (!event.getBlock().getDrops().isEmpty()){
											if (event.getBlock().getDrops().iterator()!=null){
												if (event.getBlock().getDrops().iterator().hasNext()){
									event.getBlock().getWorld().dropItemNaturally(event.getBlock().getLocation(),event.getBlock().getDrops().iterator().next());
								}
									}
								}
								}
								}
							}
						}
					}
				}
			}
		}
		
	
	}
	
	@EventHandler
	public void onBlockDamageEvent(BlockDamageEvent event)
	{
		if (event.getPlayer()!=null){
			if (event.getBlock()!=null){
				if (event.getPlayer().getInventory().getItemInMainHand()!=null){
		if(instance.getItemHandler().hasAbility(event.getPlayer().getInventory().getItemInMainHand(), "Break") && instance.getItemHandler().hasPermission(event.getPlayer(), event.getPlayer().getInventory().getItemInMainHand())){
			if(event.getBlock().getType()!=Material.BEDROCK){
				event.setInstaBreak(true);	
			}
		}
				}
			}
			
		}

	}
	
}
