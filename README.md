# Custom Items for Bukkit and Spigot #

----

[Custom Items Home Page](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=16) | [Plugin Downloads](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=17) | [API](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=18) | [Changelog](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=19) | [Internal Bug Reporting](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=25)

----

**Follow this project on Twitter [@DavidBerdik](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=6). Announcements of new releases will be posted with the [#CustomItemsPlugin](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=20) hashtag.**

----

**Like this plugin? Check out [Herobrine](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=1).**

----

**Current Release Version: v1.2.0 (compiled from commit 2269741 for Bukkit/Spigot 1.9)**

**[Download latest version now.](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=21) A complete listing of all versions of Custom Items is available under the "[Plugin Downloads](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=17)" tab.**

Custom Items is a plugin for Bukkit and Spigot-based Minecraft servers that allow server administrators to create custom crafting recipes. Although this plugin cannot be used to create entirely new items, it allows for the creation of items with custom properties (ie. teleportation bow, lightning sword). This plugin is based off of [Jakub1221's](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=12) Bukkit plugin named [CustomItems](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=22), and is released under version three of the GNU General Public License. This plugin is open source, and anyone may contribute to the plugin code.
 
### What software license is this plugin distributed under? ###

Custom Items is distributed under the GNU General Public License version 3 (GPLv3) License. The original copy of the License that was distributed with CustomItems can be found [here](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=23).

### How do I contribute? ###

To contribute to the project, fork the Custom Items repository, make whatever changes you desire to the code, and submit a pull request. I will review your changes and merge them if they are acceptable. Any changes, whether they are bug fixes or new features, are welcome.

In order to contribute, you will need to acquire Bukkit by making use of the [Spigot BuildTools](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=14). After you have compiled Bukkit, place a copy of the compiled build of Bukkit jar in the folder named 'libs' within your copy of the Eclipse workspace.

### Contribution guidelines ###

Pull requests that do not provide detail on what changes were made to the code will be denied without a review. Please provide adequate information on the changes you made to the code. **If you are planning to submit a pull request for your changes, please use [Eclipse](https://theprogrammersworld.net/adflyBitbucketRedirs.php?redirid=15) as your Java IDE to ensure that the file structure remains the same. If you use a different IDE, the file structure will be altered and I will not be able to easily merge your changes.**